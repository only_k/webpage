$(document).ready(function () {
    $('.menu-btn, .backdrop').on('click', function () {
        $('.sidebar').toggleClass('active');
    });
    $('[data-toggle="tooltip"]').tooltip()
});

// When the user clicks on <div>, open the popup
function popupFn() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
